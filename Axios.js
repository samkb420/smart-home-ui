import axios from "axios";

import store from "@/store";



const axiosClient = axios.create({
  // baseURL: "https://register-app-uat.herokuapp.com/api/v1/",
    baseURL: "https://auth-service-1.herokuapp.com/api/v1/",
});

axiosClient.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${store.state.user.token}`;
  return config;
});


export default axiosClient;


