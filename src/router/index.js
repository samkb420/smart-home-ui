import { createRouter, createWebHistory } from "vue-router";
import Home from "@/views/Home.vue";
import Dashboard from "@/pages/DashBoard.vue";
import Signup from "@/views/Signup.vue";
import Signin from "@/views/Signin.vue";
import AuthLayout from "@/Layout/AuthLayout.vue";
import DashLayout from "@/Layout/DashLayout.vue";
import Bills from "@/pages/Bills.vue";
import Enter from "@/pages/Enter.vue";
import Rooms from "@/pages/Rooms.vue";
import SecurityFeed from "@/pages/SecurityFeed.vue";
import Device from "@/pages/Device.vue";
import Schedule from "@/pages/Schedule.vue";
import Profile  from "@/pages/Profile.vue"
import SingleRoom from "@/pages/SingleRoom.vue"
import SingleEnter from "@/pages/SingleEnter.vue"


import store from '@/store'


const routes = [
  {
    name: "Home",
    path: "/",
    component: Home,
  },
  {
    path: "/dashboard",
    redirect: "/dashboard",
    component: DashLayout,
    meta: { requiresAuth: true },
    children: [
      { path: "/dashboard", name: "Dashboard", component: Dashboard },
      { path: "/bills", name: "Bills", component: Bills },
      { path: "/entertainment", name: "Entertainment", component: Enter},
      { path: "/entertainment/:id", name: "SingleEnter", component:SingleEnter},

      {path :"/rooms", name: "Rooms", component: Rooms},
      {path :"/room/:id", name:"SingleRoom" ,component:SingleRoom},
      {path:"/security", name:"Security", component: SecurityFeed},
      {path:"/device", name:"Device", component: Device},
      {path:"/schedule", name:"Schedule", component: Schedule},
      {path:"/profile",name:"Profile",component: Profile}
    ]
  },

  {
    name: "About",
    path: "/about",
    component: () => import("@/views/About.vue"),
  },
  {
    name: "Blank",
    path: "/blank",
    component: () => import("@/views/Blank.vue"),
  },

  {
    path: "/auth",
    redirect: "/login",
    name: "Auth",
    component: AuthLayout,
    meta: { isGuest: true },
    children: [
      {
        path: "/login",
        name: "Login",
        component: Signin,
      },
      {
        path: "/register",
        name: "Register",
        component: Signup,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});


  router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth && !store.state.user.token) {
      next({ name: "Login" });
    } else if (store.state.user.token && to.meta.isGuest) {
      next({ name: "Dashboard" });
    } else {
      next();
    }
  });


export default router;
