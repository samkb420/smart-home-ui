import { createStore } from "vuex";
import axios from "axios";
import {movies} from "@/data/movies";


import axiosClient from "@/../Axios";

const RoomUrl ="https://room-service-1.herokuapp.com/api/v1"

const ScheduleUrl = "https://schedule-service-1.herokuapp.com/api/v1"

export default createStore({
  state: {
    user: {
      token:sessionStorage.getItem("TOKEN"),
     
      data: {},
    },
    rooms: [],
    movies: [...movies],
    schedules: [],
  },

  mutations: {
    UPDATE_SCHEDULE(state,schedule){

      const index = state.schedules.findIndex(item => item.id === schedule.id)
      if(index !== -1){
        state.schedules.splice(index,1,schedule)
      }
    },
    UPDATE_ROOM(state, room){
      const index = state.rooms.findIndex(item => item.id === room.id)
      if(index !== -1){
          state.rooms.splice(index, 1, room)
      }
  },
  SET_SCHEDULE(state,schedules){
    state.schedules = schedules
  },

    SET_ROOM(state, rooms){

      state.rooms = rooms
  },
  DELETE_SCHEDULE(state,id){
    state.schedules = state.schedules.filter(schedule => schedule.id ==id)

  },
  DELETE_TASK(state,id){
    state.rooms = state.rooms.filter(room => room.id !== id)
  },

    SetUser: (state, user) => {
      state.user.data = user;
    },
    SetToken: (state, token) => {
      state.user.token = token;
      sessionStorage.setItem("TOKEN", token);
    },
    
    SetMovies:(state,movies)=>{
      state.movies = movies;
    },
    logout: state =>{
      state.user.data={};
      state.user.token=null;
      sessionStorage.removeItem("TOKEN");
  },

  },
  actions: {
    // room
    async getSchedule({commit}){
      const response = await axios.get(`${ScheduleUrl}/schedules`)
      console.log(response.data)
      commit("SET_SCHEDULE",response.data.data)

    },
       // add task
   async addSchedule({commit}, schedule){
    await axios.post(ScheduleUrl+"/schedules", schedule)
     .then(response => {
         commit('SET_SCHEDULE', response.data.data)
         // update tasks
         this.dispatch('getSchedule')
     })
     .catch(error => {
         console.log(error)

     })
 },
    async getRooms({commit}){
      await axios.get(RoomUrl +'/rooms')
       .then(response => {
           commit('SET_ROOM', response.data)
           console.log(response.data)
       })
       .catch(error => {
           console.log(error)

       })
   },
   // add task
   async addRoom({commit}, room){
      await axios.post(RoomUrl+'/rooms', room)
       .then(response => {
           commit('SET_ROOM', response.data.data)
           // update tasks
           this.dispatch('getRooms')
       })
       .catch(error => {
           console.log(error)

       })
   },
   // delete task
   async deleteRoom({commit}, id){
       axios.delete(RoomUrl + `/rooms/${id}`)
       .then(response => {
           commit('DELETE_TASK', id)
           dispatch("getRooms")
       })
       .catch(error => {
           console.log(error)
       })
   },
   // update task
   async updateTask({commit}, task){
       console.log(task)
     await  axios.put(baseURL +`/tasks/${task.id}`, task)
       .then(response => {
           commit('UPDATE_TASK', response.data)
           // reload tasks
           this.dispatch('getTasks')
       })
       .catch(error => {
           console.log(error)
       })
   },

   async switchlights({commit},room){
    await axios.put(RoomUrl +`/rooms/${room.id}`, room)
    .then(response => {
        commit('UPDATE_ROOM', response.data)
        // reload tasks
        this.dispatch('getRooms')
    })
    .catch(error => {
        console.log(error)
    })
   },




    //end room

    async register({ commit }, user) {
      return axiosClient.post("/register", user).then(({ data }) => {
        commit("SetUser", data.user);
        commit("setToken", data.token);
        return data;
      });
    },
    async login({ commit }, user) {
      return axiosClient.post("/login", user).then(({ data }) => {
        commit("SetUser", data);
        commit("SetToken", data.token);
        return data;
      });
    },
     fetchUser({commit}) {
      return   axiosClient.get('/profile')
      .then(res => {
        console.log(res);
        commit('SetUser', res.data)
        console.log(res.data);
      })
    },

    // set movies
    async setMovies({commit}){
      commit("SetMovies")

    },



    async logout({ commit }) {
      commit("logout");
    },
  },
  getters: {},
  modules: {},
});
